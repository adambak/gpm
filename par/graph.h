//
// Created by Szymon Matejczyk on 21.05.15.
// Modified by Adam Bak on 31.05.2015
//

#ifndef MPI_ASSIGNMENT_SEQ_GRAPH_H
#define MPI_ASSIGNMENT_SEQ_GRAPH_H

/**
 * Directed graph data structure.
 */
typedef struct {
    /**
     * For each node an array of outgoing edges.
     */
    int** edges;

    /**
     * Out-degrees for nodes or -1 if the node is not in the graph.
     */
    int* outDegrees;

    /**
     * In-degrees for nodes or -1 if the node is not in the graph
     */
    int *inDegrees;
    /**
     * Number of nodes.
     */
    int nodes;

    /**
     * Maximal id of node that has outgoing edges.
     */
    int maxNodeWithOutEdgesId;


    /**
     * Maximal id of node that has ingoing edges.
     */
    int maxNodeWithInEdgesId;

    /**
     * For each node num of a process that holds it (adjacency list).
     */
    int* nodesAssignment;
} Graph;

int isLineEmpty(const char *line);

Graph* initGraph(int nodes);

Graph* preprocessGraph(FILE* f);

Graph* readGraph(FILE* f);

void printGraph(Graph* g);

void freeGraph(Graph* graph);

Graph* reverseGraph(Graph* graph);

void readGraphString(char *s, FILE *f);

Graph* readGraphFromString(char *s);

int dfs(int node, int nextId, int parentNode, Graph* graph, Graph* reversed,
        int* numbering, int* parent, int throughReverseEdge);

#endif //MPI_ASSIGNMENT_SEQ_GRAPH_H
