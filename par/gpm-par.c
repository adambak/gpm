//
// Created by Adam Bak on 24.05.15.
//
//
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>
#include <stddef.h>

#include "common.h"
#include "graph.h"

#define MAX_MATCH_SIZE 10
#define MAX_FOUND_MATCHES 1200001 // found matches in one iteration assumption
#define MAX_PATTERN_LEN 256
#define EXCHANGE_RESULTS 0
#define EXCHANGE_REQUEST 1
#define EXCHANGE_RESPONSE 2
#define FREE_MEMORY_MB 508

/* number of available integers to keep in memory at one time */
static const int MEMORY_INT_CAPACITY = (FREE_MEMORY_MB / 4) * 1024 * 1024;

/* mpi tags */
static const int MPI_MESSAGE_RESULT_COUNT = 50;
static const int MPI_MESSAGE_RESULT = 51;
static const int MPI_MESSAGE_ADJACENCY_LIST = 52;
static const int MPI_MESSAGE_REVERSED_EDGE = 53;
static const int MPI_MESSAGE_NODE_REQUEST = 54;
static const int MPI_MESSAGE_NODE_RESPONSE = 55;

typedef struct {
    int matchedNodes;

    /**
     * `matches[i] contains data graph node id of `i` node in pattern or -1 if the
     * node is not yet matched.
     */
    int matches[MAX_MATCH_SIZE + 1];
}__attribute__((packed)) Match;

/* found matches to print */
typedef struct {
    int results;
    Match result[MAX_FOUND_MATCHES];
}__attribute__((packed)) FoundMatches;

/* queue keeps track of nodes that a process keep and that are not assigned to it */
Queue cacheQueue;
int wasExchange = 0; // is adjacency list exchange happened
int memoryIntUsedCapacity = 0; // used space of memory (num of integers)
FoundMatches computedResults;
FoundMatches recvResults;

/* derived mpi type */
MPI_Datatype MPI_MATCH_TYPE;

/* exchange of adjacency lists */
MPI_Request exchangeRequests[3];
int exchangeRecvBuf[2];
int exchangeSendBuf[2];

int ProcessesComputingNum = 1, ResultsToGet = 0;

/*
 * try to find space for a node with id = nodeNr
 * if cache queue is full or we are about to exeeed memory limit
 * that means some node has to go
 */
void manageCache(int nodeNr, Graph *dataGraph, Graph *dataReversed) {
    Graph *g = (nodeNr > 0 ? dataGraph : dataReversed);
    Graph *remG;
    KeyValuePair newCacheEntry;
    newCacheEntry.value = nodeNr; 
    if(nodeNr < 0) nodeNr = -nodeNr;
    newCacheEntry.key = -g->outDegrees[nodeNr];
    KeyValuePair qMin;
    
    while(queueIsFull(&cacheQueue) || (memoryIntUsedCapacity + g->outDegrees[nodeNr] >= MEMORY_INT_CAPACITY)) {
	qMin = queueRemoveMin(&cacheQueue);
        memoryIntUsedCapacity += qMin.key;
        remG = (qMin.value > 0 ? dataGraph : dataReversed);
        if(qMin.value < 0) qMin.value = -qMin.value;
	free(remG->edges[qMin.value]);
        remG->edges[qMin.value] = NULL;
    }

    queueInsert(&cacheQueue, newCacheEntry);
    memoryIntUsedCapacity -= newCacheEntry.key;
}

/* is node assigned to a process */
inline int isMyNode(int node, int myRank, Graph *dataGraph, Graph *dataReversed) {
    return dataGraph->nodesAssignment[node] == myRank || (dataGraph->nodesAssignment[node] == -1 && dataReversed->nodesAssignment[node] == myRank);
}

inline void initResults() {
    computedResults.results = 0;
}

void printMatch(Match* match, FILE* out) {
    fprintf(out, "%d", match->matches[1]);  
    for (int i = 2; i <= match->matchedNodes; i++) {
       fprintf(out, " %d", match->matches[i]);
    }
    fprintf(out, "\n");
}

/* add result to copmuted results */
void addResult(Match* match) {
    Match *m;
    m = &computedResults.result[computedResults.results++];
    m->matchedNodes = match->matchedNodes;
    for(int i = 1; i <= match->matchedNodes; i++) {
        m->matches[i] = match->matches[i];
    }
}

void printAllMatches(FoundMatches *ms, FILE *out) {
    for(int i = 0; i < ms->results; i++)
        printMatch(&ms->result[i], out);
}

inline int patternToGraphNode(int node, Match* match) {
    return match->matches[node];
}

inline int containsEdge(int from, int to, Graph* graph) {
    return bsearch(&to, graph->edges[from], graph->outDegrees[from], sizeof(int), lscmp) != NULL;
}

int matchContains(int nodeId, Match* match) {
    for (int i = 1; i <= MAX_MATCH_SIZE; i++) {
       if (match->matches[i] == nodeId) {
           return 1;
       }
    }
    return 0;
}

/* try to give some process adjacency list that belongs to me */
void tryPutAdjacencyList(Graph *dataGraph, Graph *dataReversed) {
    static MPI_Request *recvRequest = &exchangeRequests[EXCHANGE_REQUEST];
    MPI_Request isendRequest;
    int irecvFlag,nodeNr,procRank;
    Graph *g;

    MPI_Test(recvRequest, &irecvFlag, MPI_STATUS_IGNORE);
    while(irecvFlag) {
	nodeNr = exchangeRecvBuf[0]; procRank = exchangeRecvBuf[1];
	g = nodeNr > 0 ? dataGraph : dataReversed;
        if(nodeNr < 0) nodeNr = -nodeNr;
	MPI_Irecv(exchangeRecvBuf, 2, MPI_INT, MPI_ANY_SOURCE, MPI_MESSAGE_NODE_REQUEST, MPI_COMM_WORLD, recvRequest); // wait for another request
        if(nodeNr == 0) { //end of computations, at this point can only happen to main process
            --ProcessesComputingNum;
	} else {
	    MPI_Isend(g->edges[nodeNr], g->outDegrees[nodeNr], MPI_INT, procRank, MPI_MESSAGE_NODE_RESPONSE, MPI_COMM_WORLD, &isendRequest); // answer to the previous query
            MPI_Wait(&isendRequest, MPI_STATUS_IGNORE);
	}
	MPI_Test(recvRequest, &irecvFlag, MPI_STATUS_IGNORE);
    }
}

/* wait for another processes and respond with my adjacency lists */
void answerQueries(Graph *dataGraph, Graph *dataReversed, int myRank) {
    static MPI_Request *recvRequest = &exchangeRequests[EXCHANGE_REQUEST];
    int nodeNr,procRank;
    Graph *g;
    
    MPI_Wait(recvRequest, MPI_STATUS_IGNORE);
    while(1) {
	nodeNr = exchangeRecvBuf[0]; procRank = exchangeRecvBuf[1];
	g = nodeNr > 0 ? dataGraph : dataReversed;
        if(nodeNr < 0) nodeNr = -nodeNr;
	if(nodeNr == 0) { // end of computations
            if(myRank != 0) return;
	    --ProcessesComputingNum;
	    if(ProcessesComputingNum <= 0) return;
	} else {
	    MPI_Send(g->edges[nodeNr], g->outDegrees[nodeNr], MPI_INT, procRank, MPI_MESSAGE_NODE_RESPONSE, MPI_COMM_WORLD);
        }
        MPI_Recv(exchangeRecvBuf, 2, MPI_INT, MPI_ANY_SOURCE, MPI_MESSAGE_NODE_REQUEST, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
}

/* try to get nodeId's adjacency list and give my to others */
void exchangeAdjacencyLists(int nodeId, Graph *dataGraph, Graph *dataReversed, int checkOutEdges, int myRank) {
    static int completedRequests[2];
    static int isInit = 1;

    int gotNode = 0;
    int outcount,nodeNr;
    MPI_Request isendRequest;
    
    if(isInit) {// init waiting for requests
        MPI_Irecv(exchangeRecvBuf, 2, MPI_INT, MPI_ANY_SOURCE, MPI_MESSAGE_NODE_REQUEST, MPI_COMM_WORLD, &exchangeRequests[EXCHANGE_REQUEST]);
        wasExchange = 1;
    }
    isInit = 0;

    if(nodeId == 0) { // my computations finished
        answerQueries(dataGraph, dataReversed, myRank);	
        return;
    }
    
    Graph *g = (checkOutEdges ? dataGraph : dataReversed);
    if(g->edges[nodeId] != NULL) { // just answer some queries, you have that list
	tryPutAdjacencyList(dataGraph, dataReversed);
	return;
    }
    
    nodeNr = checkOutEdges == 1 ? nodeId : -nodeId;
    manageCache(nodeNr, dataGraph, dataReversed); // make space for thay list, if needed

    // send request
    g->edges[nodeId] = malloc(g->outDegrees[nodeId] * sizeof(int));
    exchangeSendBuf[0] = nodeNr; exchangeSendBuf[1] = myRank;
    MPI_Isend(exchangeSendBuf, 2, MPI_INT, g->nodesAssignment[nodeId], MPI_MESSAGE_NODE_REQUEST, MPI_COMM_WORLD, &isendRequest); // send request
    MPI_Wait(&isendRequest, MPI_STATUS_IGNORE);
    MPI_Irecv(g->edges[nodeId], g->outDegrees[nodeId], MPI_INT, g->nodesAssignment[nodeId], 
              MPI_MESSAGE_NODE_RESPONSE, MPI_COMM_WORLD, &exchangeRequests[EXCHANGE_RESPONSE]); // wait for response

    /* respond and wait for a response */
    while(!gotNode) {
        MPI_Waitsome(2, (exchangeRequests + 1), &outcount, completedRequests, MPI_STATUSES_IGNORE);
	if(outcount == 2 || completedRequests[0] == EXCHANGE_REQUEST - 1) {
	    tryPutAdjacencyList(dataGraph, dataReversed);
	}
        gotNode = ((outcount == 2) || (completedRequests[0] == EXCHANGE_RESPONSE - 1));
    }
}

int checkNodeMatches(int nodeData, int nodePattern, Graph* dataGraph, Graph* dataReversed, 
		     Graph* pattern, Graph* patternReversed, Match* match, int myRank) {
    if (matchContains(nodeData, match)) {
        return 0;
    }
   
    if(dataGraph->outDegrees[nodeData] > 0) 
        exchangeAdjacencyLists(nodeData, dataGraph, dataReversed, 1, myRank);
    // check out edges - every edge from the pattern must be in the graph
    for (int i = 0; i < pattern->outDegrees[nodePattern]; i++) {
        int targetPatternNode = pattern->edges[nodePattern][i];
        int target = patternToGraphNode(targetPatternNode, match);
        if (target > -1 && !containsEdge(nodeData, target, dataGraph)) {
            return 0;
        }
    }
    
    if(dataReversed->outDegrees[nodeData] > 0)
        exchangeAdjacencyLists(nodeData, dataGraph, dataReversed, 0, myRank);
    // check in edges
    for (int i = 0; i < patternReversed->outDegrees[nodePattern]; i++) {
        int sourcePatternNode = patternReversed->edges[nodePattern][i];
        int sourceData = patternToGraphNode(sourcePatternNode, match);
        if (sourceData > -1 && !containsEdge(nodeData, sourceData, dataReversed)) {
            return 0;
        }
    }

    return 1;
}

void ordering(int* numbering, int* order, int len) {
    for (int i = 1; i <= len; i++) {
        if (numbering[i] != 0) {
	    if (numbering[i] > 0) {
                order[numbering[i]] = i;
            } else {
                order[-numbering[i]] = -i;
            }
        }
    }
}

Match addNode(int node, int patternNode, Match* match) {
    Match m;
    int i;
    for (i = 0; i <= MAX_MATCH_SIZE; i++) {
        m.matches[i] = match->matches[i];
    }
    m.matches[patternNode] = node;
    m.matchedNodes = match->matchedNodes + 1;
    return m;
}

void exploreMatch(Graph* dataGraph, Graph* dataReversed, Graph* pattern, Graph* patternReversed, 
		  Match match, int* nodesMatchingOrder, int* parents, int myRank) {
    if (match.matchedNodes == pattern->nodes) {
        addResult(&match);
        return;
    }

    int checkOutEdges = 1;
    int nextNodePatternId = nodesMatchingOrder[match.matchedNodes + 1];
    if (nextNodePatternId < 0) {
        nextNodePatternId = -nextNodePatternId;
        checkOutEdges = 0;
    }
    int nextNodeParentPatternId = parents[nextNodePatternId];

    int parentId = patternToGraphNode(nextNodeParentPatternId, &match);

    // if the pattern node was visited in dfs via in edge, we use reversed graph to match it
    Graph* g = (checkOutEdges ? dataGraph : dataReversed);

    // for neighbors of parent we try to match the new node
    if(g->outDegrees[parentId] > 0) {
        for (int i = 0; i < g->outDegrees[parentId]; i++) {
	    exchangeAdjacencyLists(parentId, dataGraph, dataReversed, checkOutEdges, myRank);
	    int node = g->edges[parentId][i];
            if (checkNodeMatches(node, nextNodePatternId, dataGraph, dataReversed, pattern, 
				 patternReversed, &match, myRank)) {
                Match new = addNode(node, nextNodePatternId, &match);
                exploreMatch(dataGraph, dataReversed, pattern, patternReversed,
                             new, nodesMatchingOrder, parents, myRank);
            }
        }
    }
}

/* create mpi type for match */
void create_MPI_MATCH_TYPE(MPI_Datatype *MPI_MATCH_TYPE) {
    int blocklengths[2] = {1, MAX_MATCH_SIZE + 1};
    MPI_Datatype types[2] = {MPI_INT, MPI_INT};
    MPI_Aint  offsets[2];
    offsets[0] = offsetof(Match, matchedNodes);
    offsets[1] = offsetof(Match, matches);

    MPI_Type_create_struct(2, blocklengths, offsets, types, MPI_MATCH_TYPE);
    MPI_Type_commit(MPI_MATCH_TYPE);
}

/* distribute main graph while reading */
void distributeGraph(Graph *g, int myRank, FILE *f) {
    MPI_Status status;
    int from, to, edgesNo, maxNodeId, edgeCounter, *edgesForNode;
    char line[64];

    if(myRank == 0) {
        if(fscanf(f, "%d\n", &maxNodeId) != 1) // max node id
            error("Can't parse number of nodes.");
    
        while ((fgets(line, sizeof line, f) != NULL) && (isLineEmpty(line) == 0)) {
            sscanf(line, "%d %d", &from, &edgesNo);
            edgeCounter = 0;
            edgesForNode = malloc(edgesNo * sizeof(int));
            while (edgeCounter < edgesNo) {
                if(fgets(line, sizeof line, f) == NULL)
		    error("Can't read edge.");
                sscanf(line, "%d", &to);
                edgesForNode[edgeCounter] = to;
                edgeCounter++;
            }
            g->edges[from] = edgesForNode;

            if(g->nodesAssignment[from] != myRank) {
	        MPI_Send(g->edges[from], g->outDegrees[from], MPI_INT, g->nodesAssignment[from], 
	                 MPI_MESSAGE_ADJACENCY_LIST, MPI_COMM_WORLD);

      	        g->edges[from] = NULL;
		free(edgesForNode);
	    }
        }
    } else {
        for(int i=1; i<=g->nodes; ++i) {
	    if(g->nodesAssignment[i] == myRank && g->outDegrees[i] > 0) {
	        g->edges[i] = malloc(sizeof(int)*g->outDegrees[i]);
	        MPI_Recv(g->edges[i], g->outDegrees[i], MPI_INT, 0, MPI_MESSAGE_ADJACENCY_LIST, MPI_COMM_WORLD, &status);
	    }
	    else {
	        g->edges[i] = NULL;
	    }
	}
    }
}

/* try to receive reversed edge */
void tryRecvRevEdge(int *waiting, Graph *dataReversed, MPI_Request *recvRequest, int *recvEdge, int* counters) {
    int from, to, irecvFlag;
    MPI_Test(recvRequest, &irecvFlag, MPI_STATUS_IGNORE);
    if(irecvFlag) {
        *waiting = 0;
        from = recvEdge[0];
      	to = recvEdge[1];
        dataReversed->edges[from][counters[from]++] = to;
    }			
}

/* distribution of a reversed graph - each edge is a different message */
void distributeReversedGraph(Graph *dataGraph, Graph *dataReversed, int myRank) {
    int from, to;
    int *counters = malloc(sizeof(int) * (dataReversed->nodes + 1));
    memset(counters, 0, sizeof(int) * (dataReversed->nodes + 1));
    
    int edgesToSend = 0;
    int edgesToReceive = 0;
    /* calculate number of edges to receive and send */
    for(int i=1; i<=dataGraph->nodes; ++i) {
	if(dataReversed->nodesAssignment[i] == myRank)
	    edgesToReceive += dataReversed->outDegrees[i];

        if(dataGraph->edges[i] != NULL) {
	    for(int j=0; j<dataGraph->outDegrees[i]; ++j)
                if(dataReversed->nodesAssignment[dataGraph->edges[i][j]] == myRank)
		    --edgesToReceive;
	        else
		    ++edgesToSend;
	}
    }

    int sendEdge[2];
    int recvEdge[2];
    int waiting = 0;
    MPI_Request sendRequest,recvRequest;

    for(int i=1; i<=dataGraph->nodes; ++i) {
        if(dataGraph->edges[i] != NULL) {
            for(int j=0; j<dataGraph->outDegrees[i]; ++j) {
	        from = dataGraph->edges[i][j];
		to = i;
		if(dataReversed->nodesAssignment[from] == myRank) {
		    dataReversed->edges[from][counters[from]++] = to;
		} else {
                    sendEdge[0] = from;
		    sendEdge[1] = to;
		    MPI_Isend(&sendEdge, 2, MPI_INT, dataReversed->nodesAssignment[from], MPI_MESSAGE_REVERSED_EDGE,
		              MPI_COMM_WORLD, &sendRequest);
		    
		    /* simultaneously send and receive reversed edges */
		    MPI_Wait(&sendRequest, MPI_STATUS_IGNORE);
		    if(edgesToReceive > 0 && !waiting) {
                        MPI_Irecv(recvEdge, 2, MPI_INT, MPI_ANY_SOURCE, MPI_MESSAGE_REVERSED_EDGE, MPI_COMM_WORLD, &recvRequest);
			waiting = 1;
			--edgesToReceive;
		    } else if(waiting)
	                tryRecvRevEdge(&waiting, dataReversed, &recvRequest, recvEdge, counters);
		}
	    }
	}
    
	if(waiting)
	    tryRecvRevEdge(&waiting, dataReversed, &recvRequest, recvEdge, counters);
    }
    
    if(waiting) {
        MPI_Wait(&recvRequest, MPI_STATUS_IGNORE);
        from = recvEdge[0];
        to = recvEdge[1];
        dataReversed->edges[from][counters[from]++] = to;
    }

    while(edgesToReceive--) {
        MPI_Recv(recvEdge, 2, MPI_INT, MPI_ANY_SOURCE, MPI_MESSAGE_REVERSED_EDGE, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	from = recvEdge[0];
	to = recvEdge[1];
	dataReversed->edges[from][counters[from]++] = to;
    } 

    free(counters);

    for(int i=1; i<=dataReversed->nodes; ++i)
        if(dataReversed->edges[i] != NULL)
            qsort(dataReversed->edges[i], dataReversed->outDegrees[i], sizeof(int), lscmp);
}

/* share pointers to save some memory */
Graph *initReversedGraph(Graph* dataGraph) {
    Graph *dataReversed = malloc(sizeof(Graph));
    dataReversed->nodes = dataGraph->nodes;
    dataReversed->maxNodeWithOutEdgesId = dataGraph->maxNodeWithInEdgesId;
    dataReversed->maxNodeWithInEdgesId = dataGraph->maxNodeWithOutEdgesId;
    dataReversed->inDegrees = dataGraph->outDegrees;
    dataReversed->outDegrees = dataGraph->inDegrees;
    dataReversed->edges = malloc((dataReversed->nodes + 1) * sizeof(int*)); 
    dataReversed->nodesAssignment = malloc((dataReversed->nodes + 1) * sizeof(int));
    return dataReversed;
}

void allocateReversedEdges(Graph *dataReversed, int myRank) {
    for(int i=0; i<=dataReversed->nodes; ++i)
        if(myRank == dataReversed->nodesAssignment[i] && dataReversed->outDegrees[i] > 0)
	    dataReversed->edges[i] = malloc(sizeof(int) * dataReversed->outDegrees[i]);
	else 
 	    dataReversed->edges[i] = NULL;
}

void freeReversedGraph(Graph *dataReversed) {
    for(int i=0; i<=dataReversed->maxNodeWithOutEdgesId; ++i)
        if(dataReversed->outDegrees[i] > 0 && dataReversed->edges[i] != NULL)
	    free(dataReversed->edges[i]);

    free(dataReversed->edges);
    free(dataReversed);
}

/* evenly assign nodes to processes, based on their out degrees */
void assignNodes(Graph *g, int numProcesses) {
    memset(g->nodesAssignment, -1, sizeof(int) * (g->nodes + 1));
    KeyValuePair *nodesDegrees = malloc(sizeof(KeyValuePair) * g->maxNodeWithOutEdgesId);

    int m=-1;
    int k=0;
    for(int i=1; i<=g->maxNodeWithOutEdgesId; ++i) {
        if(g->outDegrees[i] >= 0) {
	    m = max(m, g->outDegrees[i]);
            nodesDegrees[k].key = g->outDegrees[i];
            nodesDegrees[k].value = i;
            k++;
        }
    }
    
    countingSortKeyValuePairs(&nodesDegrees, k, m);

    int p=0;
    for(int i=0; i<k; ++i) {
        g->nodesAssignment[nodesDegrees[i].value] = p;
     	p = (p+1)%numProcesses;
    }
    free(nodesDegrees);
    return;
}

/* check wheter some process has sent some matches */
void tryPrintMatches(FILE *out, MPI_Status *recvStatus) {
    static int isInit = 1;
    int irecvFlag;
    MPI_Status status;

    int gotResults = 0;
    if(recvStatus != NULL)
        MPI_Get_count(recvStatus, MPI_MATCH_TYPE, &gotResults);

    if(isInit)
	MPI_Irecv(&recvResults.result, MAX_FOUND_MATCHES, MPI_MATCH_TYPE, MPI_ANY_SOURCE, MPI_MESSAGE_RESULT, MPI_COMM_WORLD, &exchangeRequests[EXCHANGE_RESULTS]);
    isInit = 0;

    MPI_Test(&exchangeRequests[EXCHANGE_RESULTS], &irecvFlag, &status);
    while(irecvFlag) {
    	MPI_Get_count(&status, MPI_MATCH_TYPE, &recvResults.results);
	recvResults.results = max(recvResults.results, gotResults);
		printAllMatches(&recvResults, out);
	MPI_Irecv(&recvResults.result, MAX_FOUND_MATCHES, MPI_MATCH_TYPE, MPI_ANY_SOURCE, MPI_MESSAGE_RESULT, MPI_COMM_WORLD, &exchangeRequests[EXCHANGE_RESULTS]);
	--ResultsToGet;
	MPI_Test(&exchangeRequests[EXCHANGE_RESULTS], &irecvFlag, &status);
        gotResults = 0;
    }
}

// answer queries and get results from others
void waitForResultsAndRequests(FILE *out, Graph *dataGraph, Graph *dataReversed) {
    int completedRequests[2];
    int nodeNr,procRank,outcount;
    Graph *g;
    MPI_Status status[2];
    MPI_Status *resStatus;

    if(!wasExchange) {
        MPI_Irecv(exchangeRecvBuf, 2, MPI_INT, MPI_ANY_SOURCE, MPI_MESSAGE_NODE_REQUEST, MPI_COMM_WORLD, &exchangeRequests[EXCHANGE_REQUEST]);
        wasExchange = 1;
    }

    while(1) {
        MPI_Waitsome(2, exchangeRequests, &outcount, completedRequests, status); 
	if(outcount == 2 || completedRequests[0] == EXCHANGE_RESULTS) { // print result
	    resStatus = (outcount == 1 || completedRequests[0] == EXCHANGE_RESULTS ? &status[0] : &status[1]);
	    tryPrintMatches(out, resStatus);
	}
	
	if(outcount == 2 || completedRequests[0] == EXCHANGE_REQUEST) { // respond with a list
	    nodeNr = exchangeRecvBuf[0]; procRank = exchangeRecvBuf[1];
	    g = nodeNr > 0 ? dataGraph : dataReversed;
            if(nodeNr < 0) nodeNr = -nodeNr;
	    if(nodeNr == 0) { // end of computations
	        --ProcessesComputingNum;
	        if(ProcessesComputingNum <= 0) return;
	    } else {
	        MPI_Send(g->edges[nodeNr], g->outDegrees[nodeNr], MPI_INT, procRank, MPI_MESSAGE_NODE_RESPONSE, MPI_COMM_WORLD);
            }
            MPI_Irecv(exchangeRecvBuf, 2, MPI_INT, MPI_ANY_SOURCE, MPI_MESSAGE_NODE_REQUEST, MPI_COMM_WORLD, &exchangeRequests[EXCHANGE_REQUEST]);
	}
    }
}

/* all finished so main process checks for left results' messages */
void waitForAllResults(FILE *out, Graph *dataGraph, Graph *dataReversed) {
    MPI_Status status;
    while(ResultsToGet--) {
	MPI_Wait(&exchangeRequests[EXCHANGE_RESULTS], &status); 
	MPI_Get_count(&status, MPI_MATCH_TYPE, &recvResults.results);
	printAllMatches(&recvResults, out);
	MPI_Irecv(&recvResults.result, MAX_FOUND_MATCHES, MPI_MATCH_TYPE, MPI_ANY_SOURCE, MPI_MESSAGE_RESULT, MPI_COMM_WORLD, &exchangeRequests[EXCHANGE_RESULTS]);
    }
}

int main(int argc, char** argv) {
    char patternString[MAX_PATTERN_LEN];
    time_t startTime, distributionTime, computationsTime; 
    FILE *f = NULL, *out = NULL;
    int numProcesses, myRank, nodes;
    Graph *pattern, *patternReversed;
    Graph *dataGraph = NULL, *dataReversed = NULL;
    MPI_Request isendRequest;

    if (argc != 3) {
        error("Wrong number of arguments.");
    }

    startTime = time(NULL);

    MPI_Init(&argc, &argv); 
	
    create_MPI_MATCH_TYPE(&MPI_MATCH_TYPE);

    MPI_Comm_size(MPI_COMM_WORLD, &numProcesses);
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
  
    /* number of processes that are computing currently */ 
    ProcessesComputingNum = numProcesses;

    char* filename = argv[1];
    if(myRank == 0) {
        f = fopen(filename, "r");
        if (f == NULL) {
            error("Input file not found: %s\n", filename);
        }
       
        out = fopen(argv[2], "w");
        if (out == NULL) {
            error("Can't open output file.\n");
        }

        dataGraph = preprocessGraph(f);
        readGraphString(patternString, f); 
    	nodes = dataGraph->nodes;
    }

    /* send to others pattern so they can parse it */
    MPI_Bcast(&nodes, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(patternString, MAX_PATTERN_LEN, MPI_CHAR, 0, MPI_COMM_WORLD);

    pattern = readGraphFromString(patternString);
    patternReversed = reverseGraph(pattern);
    
    if(myRank == 0)
        fclose(f);
    else
        dataGraph = initGraph(nodes);
    
    /* send graph's basic info */
    MPI_Bcast(&dataGraph->maxNodeWithOutEdgesId, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dataGraph->maxNodeWithInEdgesId, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(dataGraph->outDegrees, nodes + 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(dataGraph->inDegrees, nodes + 1, MPI_INT, 0, MPI_COMM_WORLD);

    dataReversed = initReversedGraph(dataGraph);
    if(myRank == 0) {
        assignNodes(dataGraph, numProcesses);
        assignNodes(dataReversed, numProcesses);   
    }
    
    /* send nodes assignment info to others */
    MPI_Bcast(dataGraph->nodesAssignment, nodes + 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(dataReversed->nodesAssignment, nodes + 1, MPI_INT, 0, MPI_COMM_WORLD);
  
    allocateReversedEdges(dataReversed, myRank);

    if(myRank == 0) {
        f = fopen(filename, "r");
    }
    
    /* graph distribution */
    distributeGraph(dataGraph, myRank, f);

    if(myRank == 0) {
        fclose(f);
    }
        
    /* graph's transposition distribution  */
    distributeReversedGraph(dataGraph, dataReversed, myRank);
    
    int* dfsPatternNumbering = malloc((pattern->nodes + 1) * sizeof(int));
    memset(dfsPatternNumbering, 0, (pattern->nodes + 1) * sizeof(int));

    int* dfsPatternParents = malloc((pattern->nodes + 1) * sizeof(int));
    dfs(1, 1, -1, pattern, patternReversed, dfsPatternNumbering, dfsPatternParents, 0);

    int* patternNodesOrdered = malloc((pattern->nodes + 1) * sizeof(int));

    ordering(dfsPatternNumbering, patternNodesOrdered, pattern->nodes);

    MPI_Barrier(MPI_COMM_WORLD);
    distributionTime = time(NULL);
    if(myRank == 0)
        fprintf(stderr, "Distribution time[s]: %ld\n", (distributionTime - startTime));

    initResults();
    queueInitialize(&cacheQueue, 2*dataGraph->nodes+1);

    /* calculate memory usage: above queue, in/outDegrees, nodesAssignment, found and received matches */
    memoryIntUsedCapacity = 2*dataGraph->nodes + 4*dataGraph->nodes + 2*dataGraph->nodes + 2*MAX_FOUND_MATCHES*MAX_MATCH_SIZE;
    for(int i=1; i<=dataGraph->nodes; i++) 
        if(dataGraph->edges[i] != NULL) 
            memoryIntUsedCapacity += dataGraph->outDegrees[i];
    
    for(int i=1; i<=dataReversed->nodes; i++) 
	if(dataReversed->edges[i] != NULL) 
            memoryIntUsedCapacity += dataReversed->outDegrees[i];
    
    /* main loop */
    for (int i = 1; i <= dataGraph->nodes; i++) {
	if(myRank == 0) {
	    if(!(isMyNode(i, myRank, dataGraph, dataReversed)) && (dataGraph->nodesAssignment[i] != -1 || dataReversed->nodesAssignment[i] != -1))    
		++ResultsToGet;
	}
	if(isMyNode(i, myRank, dataGraph, dataReversed)) {
	    Match m;
            memset(m.matches, -1, (MAX_MATCH_SIZE + 1) * sizeof(int));
            m.matchedNodes = 1;
            m.matches[1] = i;
            exploreMatch(dataGraph, dataReversed, pattern, patternReversed, m, 
		         patternNodesOrdered, dfsPatternParents, myRank);

            if(myRank == 0) {
                printAllMatches(&computedResults, out);
                tryPrintMatches(out, NULL);
	    } else {
                MPI_Send(&computedResults.result, computedResults.results, MPI_MATCH_TYPE, 0, MPI_MESSAGE_RESULT, MPI_COMM_WORLD);
	    }
	    initResults();
	}
    }
    
    --ProcessesComputingNum;
    if(myRank != 0) { // inform the main process that this process finished
        exchangeSendBuf[0] = 0; exchangeSendBuf[1] = myRank;
        MPI_Isend(exchangeSendBuf, 2, MPI_INT, 0, MPI_MESSAGE_NODE_REQUEST, MPI_COMM_WORLD, &isendRequest);
        MPI_Wait(&isendRequest, MPI_STATUS_IGNORE);
    }

    if(ProcessesComputingNum > 0) {// respond to processes that are still computing
        if(myRank != 0)
	    exchangeAdjacencyLists(0, dataGraph, dataReversed, 1, myRank);
        else
            waitForResultsAndRequests(out, dataGraph, dataReversed);
    }
    
    if(myRank == 0) { // inform everybody about the end of computations
	waitForAllResults(out, dataGraph, dataReversed);
    	for(int i=1; i<numProcesses; ++i) {
            exchangeSendBuf[0] = 0; exchangeSendBuf[1] = myRank;
            MPI_Isend(exchangeSendBuf, 2, MPI_INT, i, MPI_MESSAGE_NODE_REQUEST, MPI_COMM_WORLD, &isendRequest);
            MPI_Wait(&isendRequest, MPI_STATUS_IGNORE); 
        }
    }

    MPI_Barrier(MPI_COMM_WORLD);
    computationsTime = time(NULL);
    if(myRank == 0)
        fprintf(stderr, "Computations time[s]: %ld\n", (computationsTime - distributionTime));

    queueDelete(&cacheQueue);
    freeGraph(pattern);
    freeGraph(patternReversed);
    free(dfsPatternNumbering);
    free(dfsPatternParents);
    free(patternNodesOrdered);
    freeReversedGraph(dataReversed); 
    freeGraph(dataGraph);
    
    if(myRank == 0) {
        fclose(out);
    }
  
    MPI_Finalize();
    return 0;
}

