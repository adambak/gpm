//
// Created by Szymon Matejczyk on 21.05.15.
// Modified by Adam Bak on 31.05.2015
//
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h> /* for isspace */

#include "graph.h"
#include "common.h"

Graph* initGraph(int nodes) {
    Graph *g = malloc(sizeof(Graph));
    g->nodes = nodes;
    g->inDegrees = malloc(sizeof(int) * (nodes + 1));
    g->outDegrees = malloc(sizeof(int) * (nodes + 1));
    g->edges = malloc(sizeof(int*) * (nodes + 1));
    g->nodesAssignment = malloc(sizeof(int) * (nodes + 1));
    return g;
}

int isLineEmpty(const char *line) {
    /* check if the string consists only of spaces. */
    while (*line != '\0')
    {
        if (isspace(*line) == 0)
            return 0;
        line++;
    }
    return 1;
}

/* read basic stats for a graph i.e. outDegrees, inDegrees */
Graph* preprocessGraph(FILE* f) {
    int from, to, edgesNo, edgeCounter, maxNodeId;
    char line[64];

    Graph* g = malloc(sizeof(Graph));
    if (fscanf(f, "%d\n", &maxNodeId) != 1) {
        error("Can't parse number of nodes.");
    }
    g->nodes = maxNodeId;
    g->edges = malloc((maxNodeId + 1) * sizeof(int*));
    g->outDegrees = malloc((maxNodeId + 1) * sizeof(int));
    g->inDegrees = malloc((maxNodeId + 1) * sizeof(int));
    g->nodesAssignment = malloc((maxNodeId + 1) * sizeof(int));

    for (int i = 0; i <= maxNodeId; i++) {
        g->outDegrees[i] = g->inDegrees[i] = -1;
        g->edges[i] = NULL;
    }

    g->maxNodeWithOutEdgesId = 0;
    g->maxNodeWithInEdgesId = 0;
    while ((fgets(line, sizeof line, f) != NULL) && (isLineEmpty(line) == 0)) {
        if (sscanf(line, "%d %d", &from, &edgesNo) == 2) {
            edgeCounter = 0;
            g->maxNodeWithOutEdgesId = max(g->maxNodeWithOutEdgesId, from);
            g->outDegrees[from] = edgesNo;

	    if(g->inDegrees[from] == -1)
                g->inDegrees[from] = 0;

            while (edgeCounter < edgesNo) {
                if (fgets(line, sizeof line, f) != NULL) {
                    if (sscanf(line, "%d", &to) == 1) {
			g->maxNodeWithInEdgesId = max(g->maxNodeWithInEdgesId, to);
                        if (g->outDegrees[to] == -1)
                            g->outDegrees[to] = 0;
                       
		        if(g->inDegrees[to] == -1)
			  g->inDegrees[to] = 0;

                        ++g->inDegrees[to];

			edgeCounter++;
                    } else {
                        error("Can't read edge from %d", from);
                    }
                } else {
                    error("Can't read edge from %d", from);
                }
            }
            debug_print("node read\n");
        } else {
            error("Can't read node\n");
        }
    }
    return g;
  
}

Graph* readGraph(FILE* f) {
    int from, to, edgesNo, edgeCounter, maxNodeId;
    char line[64];

    Graph* g = malloc(sizeof(Graph));
    if (fscanf(f, "%d\n", &maxNodeId) != 1) {
        error("Can't parse number of nodes.");
    }
    g->nodes = maxNodeId;
    g->edges = malloc((maxNodeId + 1) * sizeof(int*));
    g->outDegrees = malloc((maxNodeId + 1) * sizeof(int));
    for (int i = 0; i <= maxNodeId; i++)
        g->outDegrees[i] = -1;
    int* edgesForNode;
    g->maxNodeWithOutEdgesId = 0;
    while ((fgets(line, sizeof line, f) != NULL) && (isLineEmpty(line) == 0)) {
        if (sscanf(line, "%d %d", &from, &edgesNo) == 2) {
            edgeCounter = 0;
            g->maxNodeWithOutEdgesId = max(g->maxNodeWithOutEdgesId, from);
            g->outDegrees[from] = edgesNo;
            edgesForNode = malloc(edgesNo * sizeof(int));
            while (edgeCounter < edgesNo) {
                if (fgets(line, sizeof line, f) != NULL) {
                    if (sscanf(line, "%d", &to) == 1) {
                        debug_print("Edge %d %d\n", from, to);
                        edgesForNode[edgeCounter] = to;
                        if (g->outDegrees[to] == -1)
                            g->outDegrees[to] = 0;
                        fflush(stdout);
                        edgeCounter++;
                    } else {
                        error("Can't read edge from %d", from);
                    }
                } else {
                    error("Can't read edge from %d", from);
                }
            }
            g->edges[from] = edgesForNode;
            debug_print("node read\n");
        } else {
            error("Can't read node\n");
        }
    }
    return g;
}

/* jumps with a pointer to the new line */
char *stringJumpLine(char *s) {
    while(*s != '\n' && *s != '\0') ++s;
    if(*s != '\0') ++s;
    return s;
}

Graph* readGraphFromString(char *s) {
    int from, to, edgesNo, edgeCounter, maxNodeId;
    char line[16];

    Graph* g = malloc(sizeof(Graph));
    if (sscanf(s, "%d", &maxNodeId) != 1) {
        error("Can't parse number of nodes.");
    }  
    s = stringJumpLine(s);
    
    g->nodes = maxNodeId;
    g->edges = malloc((maxNodeId + 1) * sizeof(int*));
    g->outDegrees = malloc((maxNodeId + 1) * sizeof(int));
    g->inDegrees = NULL;
    g->nodesAssignment = NULL;
    for (int i = 0; i <= maxNodeId; i++)
        g->outDegrees[i] = -1;
    int* edgesForNode;
    g->maxNodeWithOutEdgesId = 0;
    while ((*s != '\0') && (sscanf(s, "%[^\n]s", line) != 0) && (isLineEmpty(line) == 0)) {
	if ( sscanf(line, "%d %d", &from, &edgesNo) == 2) {
            s = stringJumpLine(s);
	    edgeCounter = 0;
            g->maxNodeWithOutEdgesId = max(g->maxNodeWithOutEdgesId, from);
            g->outDegrees[from] = edgesNo;
            edgesForNode = malloc(edgesNo * sizeof(int));
            while (edgeCounter < edgesNo) {
                if (sscanf(s, "%[^\n]s", line) != 0) {
                    if (sscanf(line, "%d", &to) == 1) {
                        s = stringJumpLine(s);
                        debug_print("Edge %d %d\n", from, to);
                        edgesForNode[edgeCounter] = to;
                        if (g->outDegrees[to] == -1)
                            g->outDegrees[to] = 0;
                        fflush(stdout);
                        edgeCounter++;
                    } else {
                        error("Can't read edge from %d", from);
                    }
                } else {
                    error("Can't read edge from %d", from);
                }
            }
            g->edges[from] = edgesForNode;
            debug_print("node read\n");
        } else {
            error("Can't read node\n");
        }
    }
    return g;

}

/* reads graph description to a string */
void readGraphString(char *s, FILE *f) {
    char line[16];
    s[0] = '\0';
    while ((fgets(line, sizeof line, f) != NULL) && (isLineEmpty(line) == 0)) {
        strcat(s, line);
    } 
}

void printGraph(Graph* g) {
    printf("Graph: %d nodes, maxNodeWithOutEdgesId: %d\n", g->nodes, g->maxNodeWithOutEdgesId);
    fflush(stdout);
    for (int i = 0; i <= g->maxNodeWithOutEdgesId; i++) {
        if (g->outDegrees[i] == 0) {
            printf("Node %d with no out neighbors\n", i);
        }
        if (g->outDegrees[i] > 0) {
            printf("Node %d, out neighbors [%d]:", i, g->outDegrees[i]);
            fflush(stdout);
            for (int j = 0; j < g->outDegrees[i]; j++) {
                printf("%d ", g->edges[i][j]);
            }
            printf("\n");
            fflush(stdout);
        }
    }
}

void freeGraph(Graph* graph) {
    for (int i = 0; i <= graph->maxNodeWithOutEdgesId; i++) {
        if (graph->outDegrees[i] > 0 && graph->edges[i] != NULL) {
            free(graph->edges[i]);
        }
    }
    if(graph->edges != NULL)
      free(graph->edges);

    if(graph->outDegrees != NULL)
        free(graph->outDegrees);
    
    if(graph->inDegrees != NULL)
      free(graph->inDegrees);
    
    if(graph->nodesAssignment != NULL)
      free(graph->nodesAssignment);
    
    free(graph);
}

Graph* reverseGraph(Graph* graph) {
    Graph* reversed = malloc(sizeof(Graph));
    reversed->nodes = graph->nodes;
    int* inDegrees = malloc((graph->nodes + 1) * sizeof(int));
    for (int i = 0; i <= graph->nodes; i++) {
        inDegrees[i] = -1;
    }
    for (int i = 0; i <= graph->maxNodeWithOutEdgesId; i++) {
        if (graph->outDegrees[i] > -1 && inDegrees[i] == -1)
            inDegrees[i] = 0;
        for (int j = 0; j < graph->outDegrees[i]; j++) {
            int target = graph->edges[i][j];
            if (inDegrees[target] == -1)
                inDegrees[target] = 0;
            inDegrees[target]++;
        }
    }
    reversed->outDegrees = inDegrees;
    reversed->inDegrees = NULL;
    reversed->nodesAssignment = NULL;
    int maxNodeWithInEdges = 0;
    for (int i = 0; i <= graph->nodes; i++) {
        if (inDegrees[i] > 0)
            maxNodeWithInEdges = i;
    }
    reversed->maxNodeWithOutEdgesId = maxNodeWithInEdges;
    int** edges = malloc((maxNodeWithInEdges + 1) * sizeof(int*));
    for (int i = 0; i <= maxNodeWithInEdges; i++) {
        edges[i] = malloc(inDegrees[i] * sizeof(int));
    }
    int* counters = malloc((maxNodeWithInEdges + 1) * sizeof(int));
    memset(counters, 0, (maxNodeWithInEdges + 1) * sizeof(int));
    for (int i = 0; i <= graph->maxNodeWithOutEdgesId; i++) {
        for (int j = 0; j < graph->outDegrees[i]; j++) {
            int target = graph->edges[i][j];
            edges[target][counters[target]++] = i;
        }
    }
    free(counters);
    reversed->edges = edges;
    return reversed;
}

int dfs(int node, int nextId, int parentNode, Graph* graph, Graph* reversed,
        int* numbering, int* parent, int viaReverseEdge) {
    int currentId = nextId;
    if (viaReverseEdge) {
        numbering[node] = -currentId;
    } else {
        numbering[node] = currentId;
    }
    parent[node] = parentNode;
    for (int i = 0; i < graph->outDegrees[node]; i++) {
        if (numbering[graph->edges[node][i]] == 0) {
            currentId = dfs(graph->edges[node][i], currentId + 1, node, graph,
                            reversed, numbering, parent, 0);
        }
    }
    for (int i = 0; i < reversed->outDegrees[node]; i++) {
        if (numbering[reversed->edges[node][i]] == 0) {
            currentId = dfs(reversed->edges[node][i], currentId + 1, node, graph,
                            reversed, numbering, parent, 1);
        }
    }
    return currentId;
}

