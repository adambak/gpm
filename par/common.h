//
// Created by Szymon Matejczyk on 21.05.15.
// Modified by Adam Bak on 07.06.2015
//

#ifndef MPI_ASSIGNMENT_SEQ_COMMON_H
#define MPI_ASSIGNMENT_SEQ_COMMON_H

#ifdef DEBUG
#define DEBUG_TEST 1
#else
#define DEBUG_TEST 0
#endif

#define max(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

#define debug_print(...) \
            do { if (DEBUG_TEST) fprintf(stderr, ##__VA_ARGS__); } while (0)

typedef struct {
    int key;
    int value;
}__attribute__((packed)) KeyValuePair;

/* implementation of min queue borrowed from PWiR course on moodle */
typedef struct { 
    int n; 
    int MaxN;
    KeyValuePair *t; 
} Queue;

void error(const char* errorMsgFormat, ...);

/* compare function */
int lscmp(const void * a, const void * b);

/* in-situ counting sort of an array */
void countingSortKeyValuePairs(KeyValuePair **A, int n, int maxKey);

void queueInitialize(Queue *q, int N);

void queueDelete(Queue *q);

void queueInsert(Queue *q, KeyValuePair x);

KeyValuePair queueRemoveMin(Queue *q);

int queueIsFull(Queue *q);

#endif //MPI_ASSIGNMENT_SEQ_COMMON_H
