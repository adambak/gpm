//
// Created by Szymon Matejczyk on 21.05.15.
// Modified by Adam Bak on 07.06.2015
//
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"

void error(const char* errorMsgFormat, ...) {
    va_list argptr;
    va_start(argptr, errorMsgFormat);
    vfprintf(stderr, errorMsgFormat, argptr);
    fflush(stderr);
    va_end(argptr);
    exit(-1);
}

int lscmp(const void * a, const void * b) {
  return (*(int*)a - *(int*)b);
}

/* performs counting sort on an array of key-value pairs, n elements */
void countingSortKeyValuePairs(KeyValuePair **A, int n, int maxKey) {
    KeyValuePair *In = *A;
    KeyValuePair *Out = (KeyValuePair *)malloc(sizeof(KeyValuePair) * n);
    
    int *C = (int *)malloc(sizeof(int) * (maxKey + 1));
    memset(C, 0, sizeof(int) * (maxKey + 1));

    for(int i=0; i<n; ++i)
        ++C[In[i].key];

    for(int i=1; i<=maxKey; ++i)
        C[i] += C[i-1];

    for(int i=n-1; i>=0; --i)
        Out[--C[In[i].key]] = In[i];
   
    *A = Out;
    free(In);
    free(C);
}

void queueInitialize(Queue *q, int N) {
    q->n = 1;
    q->MaxN = N;
    q->t = malloc(sizeof(KeyValuePair) * (N + 1));
}

void queueDelete(Queue *q) {
    free(q->t);
}

void queueInsert(Queue *q, KeyValuePair x) {
    int f, i;
    KeyValuePair v;
    for (i = q->n++; (f = i / 2) && (v = q->t[f]).key > x.key; i = f)
        q->t[i] = v;
    q->t[i] = x;
}

KeyValuePair queueRemoveMin(Queue *q) {
    int i, s;
    KeyValuePair r, x, v, w;
    r = q->t[1];
    if (--q->n > 1) {
        x = q->t[q->n];
        for (i = 1; (s = 2 * i) < q->n; i = s) {
            v = q->t[s];
            if (s + 1 < q->n && (w = q->t[s + 1]).key < v.key) {
                s++;
                v = w;
            }
            if (v.key >= x.key)
                break;
            q->t[i] = v;
        }
        q->t[i] = x;
    }
    return r;
}

int queueIsFull(Queue *q) {
    return q->n >= q->MaxN;
}

