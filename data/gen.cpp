#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

const int MAXN = 26;
int M[MAXN][MAXN];

int main(int argc, char *argv[]) {
	int x,y;
	srand(time(NULL));
	
	int m = 121;
	while(m--) {
		do {
			x = rand()%(MAXN-1) + 1;
			y = rand()%(MAXN-1) + 1;
		} while(x == y);
		M[x][y] = 1;
	}

	cout << MAXN - 1 << endl;
	for(int i=1; i < MAXN; i++) {
		int c = 0;
		for(int j=1; j<MAXN; j++) if(M[i][j] == 1) c++;
		if(c > 0) {
			cout << i << " " << c << endl;
			for(int j=1; j<MAXN; j++)
				if(M[i][j] == 1) cout << j << endl;
		}
	}

	cout << endl;

	// pattern
	cout << "3" << endl;
	cout << "1 1" << endl;
	cout << "2" << endl;
	cout << "2 1" << endl;
	cout << "3" << endl;
	cout << "3 1" << endl;
	cout << "1" << endl;

	return 0;
}

