#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

const int MAXN = 20001;
int M[MAXN][MAXN];

int main(int argc, char *argv[]) {
	int x,y;
	srand(time(NULL));

	int m = 5000001;
	while(m--) {
		do {
			x = rand()%(MAXN-1) + 1;
			y = rand()%(MAXN-1) + 1;
		} while(x == y);
		M[x][y] = 1;
	}

/*
	for(int i=1; i<MAXN; i++)
		for(int j=1; j<MAXN; j++)
			if(i!=j)M[i][j]=1;
*/
	cout << MAXN - 1 << endl;
	for(int i=1; i < MAXN; i++) {
		int c = 0;
		for(int j=1; j<MAXN; j++) if(M[i][j] == 1) c++;
		if(c > 0) {
			cout << i << " " << c << endl;
			for(int j=1; j<MAXN; j++)
				if(M[i][j] == 1) cout << j << endl;
		}
	}

	cout << endl;

	// pattern
	
	cout << "8" << endl;
	cout << "1 3" << endl;
	cout << "2" << endl;
	cout << "3" << endl;
	cout << "4" << endl;
	cout << "2 2" << endl;
	cout << "1" << endl;
	cout << "5" << endl;
	cout << "3 3" << endl;
	cout << "5" << endl;
	cout << "6" << endl;
	cout << "7" << endl;
	cout << "4 3" << endl;
	cout << "5" << endl;
	cout << "6" << endl;
	cout << "7" << endl;
	cout << "6 4" << endl;
	cout << "1" << endl;
        cout << "4" << endl;
	cout << "5" << endl;	
	cout << "8" << endl;
        

	return 0;
}

